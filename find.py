#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys


def lookup(path, key):
    """Lookup a value within a key:value file."""
    with open(path, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            if line.startswith('#'):
                continue
            info = line.split(':')
            if len(info) == 2 and info[0] == key:
                return info[1]
    return "N/A"


if __name__ == "__main__":
    if not len(sys.argv) == 3:
        print("USAGE: find.py file key")
    print(lookup(sys.argv[1], sys.argv[2]))
